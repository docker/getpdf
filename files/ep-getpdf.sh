#!/bin/bash

# Prise en compte des variables / application des valeurs par défaut
eval . /var/www/html/config/tuning.conf.tmpl
set | grep ^GETPDF__ > /var/www/html/config/tuning.conf
chmod 644 /var/www/html/config/tuning.conf
rm -f /var/www/html/config/tuning.conf.tmpl

# Ensure permissions in case of volumes are used :
chown -R ${WEB_USERNAME:-www-data}    /var/www/html/logs /var/www/html/docs

