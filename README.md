# Getpdf : convertit une URL en PDF ou Image

## Variables d'environnement

- toutes celles permises par l'image "httpd:2.4" (voir sur le même registre)

- GETPDF__ARCHIVELOG : (10080) délai en minutes de conservation du log général.

- GETPDF__ARCHIVEDEBUG : (720) délai en minutes de conservation du log de debug des requêtes.

- GETPDF__ARCHIVEDOC : (720) délai en minutes de conservation des documents produits.

- GETPDF__DISPLAY_USAGE_ON_ERROR : (true) affichage des messages d'erreur.

- GETPDF__ENABLE_ACL : (false) utiliser les ACL ou pas.

- GETPDF__WKHTMLTODOC_OPTIONS : vide par défaut.



## Contenu de ce dossier

- Makefile
- docker-compose.yml
- config/
- .gitlab-ci.yml

### Structure de la configuration

```
config-ademe/
├── acl.conf
├── main.conf
├── tuning.conf
└── options
    ├── default-image.conf
    ├── default-pdf.conf
    ├── paysage.conf
    ├── portrait.conf
    ├── paysage.conf
    └── other...*.conf (à monter dans le conteneur)
```

### Utilisation

Adapter le fichier **docker-compose.yml**.

```
version: '2.4'

services:
  web:
    image: registry.actilis.net/docker-images/getpdf:bookworm-latest
    restart: on-failure
    hostname: getpdf.local
    ports:
    - 8000:80
    # volumes:
    # - /srv/docker/pdf.actilis.net/logs:/var/www/html/logs
    # - /srv/docker/pdf.actilis.net/docs:/var/www/html/docs
    environment:
      - HOSTNAME=getpdf.local
```

Lancer le conteneur :

```
$ docker-compose up -d
```


### Exploitation

- Les documents demandés sont générés dans /var/www/html/data, qui est purgé automatiquement 
  ==> variable d'environnement : GETPDF__ARCHIVEDOC=60

- Les logs Applicatifs sont générés dans /var/www/html/logs, qui est purgé automatiquement
  ==> variable d'environnement : GETPDF__ARCHIVELOG=10080

  Les logs de debuggage sont aussi dans ce répertoire, et purgés autmatiquement (10 minutes)
  ==> variable d'environnement : GETPDF__ARCHIVEDEBUG=10

- Les logs d'Apache sont émis sur stderr et stdout


### Volumes

Si le service doit être déployé en plusieurs répliques, il est raisonnable d'utiliser des volumes communs (exemple dans docker-compose.yml) ou un dispositif d'affinité de session dans Traefik.



